import os
import logging
import numpy as np
from sklearn.svm import SVC, LinearSVC
from util import load_mnist, dump_model, load_model

logging.basicConfig(format='%(asctime)s - %(levelname)s: %(message)s', datefmt='%d/%m/%Y %I:%M:%S %p', level=logging.INFO)

def main():
    X_train, y_train = load_mnist("train", "./data/")
    X_test, y_test = load_mnist("t10k", "./data/")
    X_train = np.reshape(X_train, (-1, 784))
    X_test = np.reshape(X_test, (-1, 784))

    model_filename = "linear_svc.pkl"
    svc = load_model(model_filename)
    if svc is None:
        logging.info("Traing Linear SVC model")
        # Choosing the dot-product i.e. linear kernel,
        # with L1 loss, and solving the primal problem instead
        # of the dual, as num_samples > num_features (~60000 > 784)
        # For multi-class problems, this packages uses a one-vs-rest method,
        # as it computationally efficient, compared to other methods like
        # one-vs-one, or Crammer-Singer
        svc = LinearSVC(penalty="l1", C=2.0, dual=False, verbose=True)
        svc.fit(X_train, y_train)
        dump_model(svc, model_filename)
        logging.info("Saved trained model as {0}".format(model_filename))
    else:
        logging.info("Using existing model from {0}".format(model_filename))
    accuracy = svc.score(X_test, y_test)
    logging.info("Accuracy of Support Vector Classifier (Linear kernel) with MNIST test data: {0}".format(accuracy))
    return

if __name__ == "__main__":
    main()
