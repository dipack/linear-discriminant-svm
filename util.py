import os
import pickle
import numpy as np

def load_mnist(prefix, folder):
    int_type = np.dtype(np.int32).newbyteorder('>')
    n_meta_data_bytes = 4 * int_type.itemsize

    img_file_name = "{0}/{1}-images-idx3-ubyte".format(folder, prefix)
    data = np.fromfile(img_file_name, dtype='ubyte')
    magic_bytes, n_images, width, height = np.frombuffer(data[:n_meta_data_bytes].tobytes(), int_type)
    data = data[n_meta_data_bytes:].astype(dtype=np.float32).reshape([n_images, width, height])

    labels_file_name = "{0}/{1}-labels-idx1-ubyte".format(folder, prefix)
    labels = np.fromfile(labels_file_name, dtype='ubyte')[2 * int_type.itemsize:]

    return data, labels

def dump_model(model, model_filename="model.pkl"):
    with open(model_filename, "wb") as fi:
        pickle.dump(model, fi)
    return True

def load_model(model_filename="model.pkl"):
    model = None
    if os.path.isfile(model_filename):
        with open(model_filename, "rb") as fi:
            model = pickle.load(fi)
    return model
