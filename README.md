# CSE 555 - Linear Discriminant Functions & Support Vector Machines
## Second Assignment
### How to Run

1. Problem 1: You can run the code for this problem as follows: `python3 first.py`. This requires that the file `util.py` be in the same directory as `first.py`, as well as `numpy`, `scikit-learn` packages.

NOTE: The MNIST image data from LeCun's web page must be within the `./data/` folder, unzipped.

